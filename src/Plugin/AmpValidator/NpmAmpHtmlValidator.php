<?php

namespace Drupal\amp_validator\Plugin\AmpValidator;

use Drupal\amp_validator\Annotation\AmpValidatorPlugin;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\StringTranslation\TranslationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\amp_validator\Plugin\AmpValidatorPluginBase;

/**
 * Cloudflare AmpValidator plugin.
 *
 * @AmpValidatorPlugin(
 *  id = "npm_amp_html_validator",
 *  label = @Translation("NPM AMP HTML Validator"),
 *  description=@Translation("AMP Validator provided by NPM AMP HTML Validator")
 * )
 *
 * @package Drupal\amp_validator\Plugin\AmpValidator
 */
class NpmAmpHtmlValidator extends AmpValidatorPluginBase {

  /**
   * @param \GuzzleHttp\ClientInterface $http_client
   *   A Guzzle client object.
   *
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, TranslationInterface $string_translation, ConfigFactory $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $string_translation, $config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('string_translation'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function validate($type = 'url') {
    if (!empty($this->data)) {

      try {
        exec('/usr/bin/amphtml-validator --format json ' . $this->data->toString(), $output, $return_value);

        if ($return_value == 0) {
          $this->valid = TRUE;
        }

        $output = Json::decode($output[0]);
        foreach ($output[$this->data->toString()]['errors'] as $error) {
          $this->errors[] = [
            'code' => $error['code'],
            'info' => $error['message'],
            'line' => $error['line'],
            'col' => $error['col'],
            'help' => $error['specUrl'] ?? '',
          ];
        }
      } catch (\Exception $e) {
          watchdog_exception('amp_validator', $e);
      }
    }
  }

}
